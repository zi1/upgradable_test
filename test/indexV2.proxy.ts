import chai, { expect } from 'chai';
import ChaiAsPromised from 'chai-as-promised';
import { ethers, upgrades } from 'hardhat';
import TokenConfig from '../config/TokenConfig';
import { TokenContractType } from '../lib/ContractProvider';
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';

chai.use(ChaiAsPromised);

describe(TokenConfig.contractName, function () {
  let owner!: SignerWithAddress;
  let holder!: SignerWithAddress;
  let externalUser!: SignerWithAddress;
  let contractV1!: TokenContractType;
  let contract!: TokenContractType;

  before(async function () {
    [owner, holder, externalUser] = await ethers.getSigners();
  });

  it('Contract proxy deployment', async function () {
    const ContractV1 = await ethers.getContractFactory('UpgradesTest');
    const Contract = await ethers.getContractFactory(TokenConfig.contractName);
    contractV1 = await upgrades.deployProxy(ContractV1, [42], { initializer: 'store' }) as TokenContractType;
    contract = await upgrades.upgradeProxy(contractV1.address, Contract) as TokenContractType;
  });

  it('retrieve returns a value previously incremented', async function () {
    await contract.increment();

    expect((await contract.retrieve()).toString()).to.equal('43');
  });
});
