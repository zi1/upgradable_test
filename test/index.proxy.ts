import chai, { expect } from 'chai';
import ChaiAsPromised from 'chai-as-promised';
import { ethers, upgrades } from 'hardhat';
import TokenConfig from '../config/TokenConfig';
import { TokenContractType } from '../lib/ContractProvider';
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';

chai.use(ChaiAsPromised);

describe('UpgradesTest', function () {
  let owner!: SignerWithAddress;
  let holder!: SignerWithAddress;
  let externalUser!: SignerWithAddress;
  let contract!: TokenContractType;

  before(async function () {
    [owner, holder, externalUser] = await ethers.getSigners();
  });

  it('Contract proxy deployment', async function () {
    const Contract = await ethers.getContractFactory('UpgradesTest');
    contract = await upgrades.deployProxy(Contract, [42], { initializer: 'store' }) as TokenContractType;
  });

  it('retrieve returns a value previously initialized', async function () {
    expect((await contract.retrieve()).toString()).to.equal('42');
  });
});
