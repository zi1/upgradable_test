import TokenConfigInterface from '../lib/TokenConfigInterface';
import * as Networks from '../lib/Networks';

const TokenConfig: TokenConfigInterface = {
  testnet: Networks.bscTestnet,
  mainnet: Networks.bscMainnet,
  contractName: 'UpgradesTestV2',
  contractAddress: '0x21408241393Fc9E05415e5Fc68F68E8ED0BD00bA',
  proxyAdminAddress: '0xDBE1fb06CE9eff176a24c1C4506130a93682e4B5',
  multiSigAddress: '',
};

export default TokenConfig;
